# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/07 09:26:14 by dvdovenk          #+#    #+#              #
#    Updated: 2017/02/07 09:29:29 by dvdovenk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC = ft_printf.c \
		srcs/ft_base16.c \
		srcs/ft_string.c \
		srcs/ft_char.c  \
		srcs/ft_checkers.c \
		srcs/ft_counters.c \
		srcs/ft_num8.c \
		srcs/ft_num10.c \
		srcs/ft_num16.c \
		srcs/ft_unsigned.c \
		srcs/ft_tools.c \
		srcs/ft_create_new_list.c \
		srcs/ft_atoi.c \
		srcs/ft_strdel.c \
		srcs/ft_strnew.c \
		srcs/ft_strjoin.c \
		srcs/ft_pointer.c \
		srcs/ft_output.c \
		srcs/ft_wchar.c \
		srcs/ft_toolkit.c \
		srcs/ft_color.c \
		srcs/ft_binary.c 

OBJ = $(SRC:.c=.o)

HEADER = ft_libftprintf.h

CC = gcc

CFLAGS = -c -Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c $(HEADER)
		$(CC) $(CFLAGS) -o $@ $<

clean:
		rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: clean all

