/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 18:35:18 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/25 18:35:19 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

void				ft_output(char *str)
{
	if (str)
	{
		while (*str)
		{
			write(1, str, 1);
			str++;
		}
	}
}

int					ft_strcmp(char const *s1, char const *s2)
{
	unsigned char	*src;
	unsigned char	*dst;
	int				i;

	src = (unsigned char*)s1;
	dst = (unsigned char*)s2;
	i = 0;
	while (src[i] == dst[i] && dst[i] != '\0')
		i++;
	if (src[i] == '\0' && dst[i] == '\0')
		return (0);
	return (src[i] - dst[i]);
}

void				ft_put_color(int i)
{
	char			**tmp;

	tmp = (char**)malloc(sizeof(char*) * 18);
	tmp[0] = BLACK;
	tmp[1] = RED;
	tmp[2] = GREEN;
	tmp[3] = BROWN;
	tmp[4] = BLUE;
	tmp[5] = MAGENTA;
	tmp[6] = CYAN;
	tmp[7] = GRAY;
	tmp[8] = DARK_GRAY;
	tmp[9] = LIGHT_RED;
	tmp[10] = LIGHT_GREEN;
	tmp[11] = LIGHT_YELLOW;
	tmp[12] = LIGHT_BLUE;
	tmp[13] = LIGHT_MAGENTA;
	tmp[14] = LIGHT_CYAN;
	tmp[15] = WHITE;
	tmp[16] = EOC;
	tmp[17] = NULL;
	ft_output(tmp[i]);
}

char				**ft_colors(void)
{
	char			**tmp;

	tmp = (char**)malloc(sizeof(char*) * 18);
	tmp[0] = BLACK_COLOR;
	tmp[1] = RED_COLOR;
	tmp[2] = GREEN_COLOR;
	tmp[3] = BROWN_COLOR;
	tmp[4] = BLUE_COLOR;
	tmp[5] = MAGENTA_COLOR;
	tmp[6] = CYAN_COLOR;
	tmp[7] = GRAY_COLOR;
	tmp[8] = DARK_GRAY_COLOR;
	tmp[9] = LIGHT_RED_COLOR;
	tmp[10] = LIGHT_GREEN_COLOR;
	tmp[11] = LIGHT_YELLOW_COLOR;
	tmp[12] = LIGHT_BLUE_COLOR;
	tmp[13] = LIGHT_MAGENTA_COLOR;
	tmp[14] = LIGHT_CYAN_COLOR;
	tmp[15] = WHITE_COLOR;
	tmp[16] = EOC_COLOR;
	tmp[17] = NULL;
	return (tmp);
}

int					ft_set_color(const char *data, int pos, int i)
{
	char			*color;
	char			**set;

	i = pos;
	while (data[pos] && data[pos] != '}')
		pos++;
	color = ft_strsub(data, i, pos - i + 1);
	set = ft_colors();
	i = 0;
	while (i < 17)
	{
		if (ft_strcmp(color, set[i]) == 0)
		{
			ft_put_color(i);
			return (ft_strlen(color));
		}
		i++;
	}
	ft_putchar('{');
	return (1);
}
