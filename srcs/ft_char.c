/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_char.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 21:36:31 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/07 21:36:32 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

void		ft_putchar_check(t_desc *list, va_list arg)
{
	int		c;

	c = va_arg(arg, int);
	if (list->flag_min == 1)
	{
		ft_putchar(c);
		ft_place_shift(list->width - 1);
	}
	else if (list->flag_min == 0 && list->flag_zero == 1)
	{
		ft_putstr(ft_place_zero(list->width - 1));
		ft_putchar(c);
	}
	else if (list->flag_min == 0 && list->flag_zero == 0)
	{
		ft_place_shift(list->width - 1);
		ft_putchar(c);
	}
	else
		ft_putchar(c);
}
