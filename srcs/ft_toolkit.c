/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toolkit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 18:42:35 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/23 18:42:36 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

void				ft_putnbr(unsigned long long n)
{
	if (n >= 10)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
		ft_putchar(n + '0');
}

void				ft_check_width(char *data, t_desc *list)
{
	int				i;
	int				tmp;

	i = 0;
	if ((tmp = ft_last_flag(data)) != -1)
	{
		i = tmp;
		while (data[i] >= '0' && data[i] <= '9')
			i--;
		if (data[i] == '.')
			list->width = 0;
		else
			list->width = ft_printf_atoi(data, tmp);
	}
}

void				ft_put_space(t_desc *list, intmax_t num)
{
	if (list->prec >= list->width)
		ft_putchar(' ');
	else if ((int)ft_numlen(num) > list->prec &&
		(int)ft_numlen(num) > list->width)
		ft_putchar(' ');
	else if ((list->width > list->prec) && (list->width > (int)ft_numlen(num)))
		ft_putchar(' ');
}

char				*ft_strsub(char const *s, unsigned int start, size_t len)
{
	unsigned int	i;
	char			*d;

	i = 0;
	d = NULL;
	if (!s)
		return (0);
	d = (char *)malloc(sizeof(char) * len + 1);
	if (d)
	{
		while (i < len)
		{
			d[i] = s[start];
			i++;
			start++;
		}
		d[i] = '\0';
		return (d);
	}
	else
		return (NULL);
}

void				ft_n_check(va_list arg)
{
	int				*c;

	c = va_arg(arg, int*);
	*c = g_counter;
}
