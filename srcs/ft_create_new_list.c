/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_new_list.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 15:20:13 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/07 15:20:44 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

static	char	*printing(int n, char *d, int sign, int k)
{
	d[k] = '\0';
	if (n == 0)
		d[0] = 48;
	if (sign == 1)
	{
		sign = 0;
		d[0] = '-';
		d[--k] = (char)((n % 10) + 48 + 1);
		n = n / 10;
	}
	while (n > 0)
	{
		d[--k] = (char)((n % 10) + 48);
		n = n / 10;
	}
	return (d);
}

char			*ft_itoa(int n)
{
	int			k;
	int			sign;
	char		*d;
	int			i;

	k = 0;
	sign = 0;
	if (n < 0)
	{
		sign = 1;
		n = (-n) - 1;
		k++;
	}
	i = n;
	if (i == 0)
		k++;
	while (i > 0)
	{
		i = i / 10;
		k++;
	}
	if (!(d = (char *)malloc(sizeof(char) * k + 1)))
		return (NULL);
	return (printing(n, d, sign, k));
}

t_desc			*ft_create_new_list(void)
{
	t_desc		*tmp;

	tmp = (t_desc*)malloc(sizeof(t_desc));
	if (tmp)
	{
		tmp->flag_min = 0;
		tmp->flag_plus = 0;
		tmp->flag_space = 0;
		tmp->flag_zero = 0;
		tmp->flag_hash = 0;
		tmp->width = 0;
		tmp->prec = -2;
		tmp->length = 0;
		tmp->spec = '-';
	}
	return (tmp);
}
