/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 17:33:41 by dvdovenk          #+#    #+#             */
/*   Updated: 2016/12/01 12:14:28 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

char		*ft_strjoin(const char *s1, const char *s2)
{
	int		k;
	int		i;
	char	*d;

	if (!s1 || !s2)
		return (NULL);
	k = 0;
	i = 0;
	d = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (d)
	{
		while (s1[i] != '\0')
			d[k++] = s1[i++];
		i = 0;
		while (s2[i] != '\0')
			d[k++] = s2[i++];
		d[k] = '\0';
	}
	return (d);
}

char		*ft_strjoincl(char *str1, char *str2)
{
	char	*res;

	res = ft_strjoin(str1, str2);
	return (res);
}
