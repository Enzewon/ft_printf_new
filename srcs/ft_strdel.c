/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 17:58:40 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/01/26 20:35:35 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

void		ft_strdel(char **as)
{
	if (as)
	{
		free(*as);
		*as = NULL;
	}
}

void		ft_strdel_array(char **data)
{
	int		i;

	i = 0;
	while (data[i])
	{
		ft_strdel(&data[i]);
		i++;
	}
	free(data);
}

void		ft_strdel_16(char *data)
{
	int		i;
	int		check;

	i = 0;
	check = 0;
	while (data[i])
	{
		if (data[i] != '0' && data[i] != 'x' && data[i] != 'X')
			check = 1;
		i++;
	}
	if (check == 1)
		ft_strdel(&data);
}
