/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 16:21:52 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/23 16:21:54 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_libftprintf.h"

void		ft_putwstr(char **s)
{
	int		i;
	int		j;

	i = 0;
	if (s)
		while (s[i])
		{
			j = 0;
			while (s[i][j] != '\0')
			{
				ft_putchar(s[i][j]);
				j++;
			}
			i++;
		}
}

void		ft_putstr(char *s)
{
	if (s)
		while (*s)
		{
			ft_putchar(*s);
			s++;
		}
}

void		ft_putchar(int c)
{
	write(1, &c, 1);
	g_counter++;
}

int			ft_wstrlen(char **str)
{
	int	i;
	int	tmp;

	i = 0;
	tmp = 0;
	while (str[i])
	{
		tmp += ft_strlen(str[i]);
		i++;
	}
	return (tmp);
}

void		ft_put_zero(int size)
{
	int		j;

	j = 0;
	while (j < size)
	{
		ft_putchar('0');
		j++;
	}
}
