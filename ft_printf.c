/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 15:51:41 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/07 14:32:37 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_libftprintf.h"

void		ft_putnoarg(t_desc *list)
{
	char	c;

	c = list->spec;
	if (list->flag_min == 1)
	{
		ft_putchar(c);
		ft_place_shift(list->width - 1);
	}
	else if (list->flag_min == 0 && list->flag_zero == 1)
	{
		ft_putstr(ft_place_zero(list->width - 1));
		ft_putchar(c);
	}
	else if (list->flag_min == 0 && list->flag_zero == 0)
	{
		ft_place_shift(list->width - 1);
		ft_putchar(c);
	}
}

void		ft_before_print(t_desc *list, va_list arg)
{
	if (list->spec == 'd' || list->spec == 'D' || list->spec == 'i')
		ft_regular_num(list, arg);
	else if (list->spec == 'o' || list->spec == 'O')
		ft_base_8(list, arg);
	else if (list->spec == 'x' || list->spec == 'X')
		ft_base_16(list, arg);
	else if (list->spec == 'u' || list->spec == 'U')
		ft_unsigned_check(list, arg);
	else if (list->spec == 's' || list->spec == 'S')
		ft_putstr_check(list, arg);
	else if (list->spec == 'c' || list->spec == 'C')
		ft_putchar_check(list, arg);
	else if (list->spec == 'p')
		ft_pointer_check(list, arg);
	else if (list->spec == 'n')
		ft_n_check(arg);
	else if (list->spec == 'b')
		ft_binary(list, arg);
	else
		ft_putnoarg(list);
}

int			ft_begin(const char *str, int i, va_list ar)
{
	int		count;
	char	*data;
	t_desc	*list;

	count = 0;
	list = ft_create_new_list();
	data = ft_takestring(str, i, list);
	count += ft_strlen(data);
	ft_check_flag(data, list);
	ft_check_width(data, list);
	ft_check_prec(data, list);
	ft_check_star(data, list, ar, 0);
	ft_check_length(data, list);
	if (list->spec == '\0')
		return (count);
	ft_before_print(list, ar);
	ft_strdel(&data);
	free(list);
	return (count + 1);
}

int			ft_printf(const char *format, ...)
{
	va_list	arg;
	int		i;

	i = 0;
	g_counter = 0;
	va_start(arg, format);
	while (format[i] != '\0')
	{
		if (format[i] == '{')
			i += ft_set_color(format, i, 0);
		else if (format[i] == '%')
		{
			i++;
			i += ft_begin(format, i, arg);
			if (format[i] == '\0')
				return (g_counter);
		}
		else
		{
			ft_putchar(format[i]);
			i++;
		}
	}
	va_end(arg);
	return (g_counter);
}
