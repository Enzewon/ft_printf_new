/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_libftprintf.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvdovenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 16:20:21 by dvdovenk          #+#    #+#             */
/*   Updated: 2017/02/01 16:43:52 by dvdovenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIBFTPRINTF_H
# define FT_LIBFTPRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>
# include <wchar.h>
# define BLACK               "\033[22;30m"
# define BLACK_COLOR         "{color:black}"
# define RED                 "\033[22;31m"
# define RED_COLOR           "{color:red}"
# define GREEN               "\033[22;32m"
# define GREEN_COLOR         "{color:green}"
# define BROWN               "\033[22;33m"
# define BROWN_COLOR         "{color:brown}"
# define BLUE                "\033[22;34m"
# define BLUE_COLOR          "{color:blue}"
# define MAGENTA             "\033[22;35m"
# define MAGENTA_COLOR       "{color:magenta}"
# define CYAN                "\033[22;36m"
# define CYAN_COLOR          "{color:cyan}"
# define GRAY                "\033[22;37m"
# define GRAY_COLOR          "{color:gray}"
# define DARK_GRAY           "\033[01;30m"
# define DARK_GRAY_COLOR     "{color:dark gray}"
# define LIGHT_RED            "\033[01;31m"
# define LIGHT_RED_COLOR     "{color:light red}"
# define LIGHT_GREEN         "\033[01;32m"
# define LIGHT_GREEN_COLOR    "{color:light green}"
# define LIGHT_YELLOW           "\033[01;33m"
# define LIGHT_YELLOW_COLOR  "{color:light yellow}"
# define LIGHT_BLUE             "\033[01;34m"
# define LIGHT_BLUE_COLOR    "{color:light blue}"
# define LIGHT_MAGENTA          "\033[01;35m"
# define LIGHT_MAGENTA_COLOR "{color:light magenta}"
# define LIGHT_CYAN             "\033[01;36m"
# define LIGHT_CYAN_COLOR    "{color:light cyan}"
# define WHITE                  "\033[01;37m"
# define WHITE_COLOR         "{color:white}"
# define EOC                    "\033[0m"
# define EOC_COLOR           "{color:eoc}"
# define BIT7 127
# define BIT11 2047
# define BIT16 65535
# define BIT21 2097151

int				g_counter;

typedef struct	s_desc
{
	int			flag_min;
	int			flag_plus;
	int			flag_space;
	int			flag_zero;
	int			flag_hash;
	int			width;
	int			prec;
	int			length;
	char		spec;
}				t_desc;

void			ft_put_space(t_desc *list, intmax_t num);
void			ft_wchar_null(char *data, t_desc *list);
void			ft_go_wstr(t_desc *list, wchar_t *data);
void			ft_strdel_array(char **data);
size_t			ft_len_num(uintmax_t value, int base);
void			ft_putnbr(unsigned long long n);
void			ft_binary(t_desc *list, va_list arg);
int				ft_set_color(const char *data, int pos, int i);
char			*ft_zeroing_string(char *data, t_desc *list);
char			*ft_makestr_prec(char *str, t_desc *list);
void			ft_put_zero(int size);
int				ft_take_wlen(uint32_t symbol);
int				ft_wstrlen(char **str);
void			ft_check_width(char *data, t_desc *list);
void			ft_n_check(va_list arg);
void			ft_putwstr(char **s);
void			ft_putchar(int c);
int				ft_check_content(char *data);
void			ft_strdel_16(char *data);
void			ft_check_star(char *data, t_desc *list, va_list arg, int tmp);
char			*ft_set_star(char *str, va_list arg);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_lower_case(char *str);
void			ft_flag_set(char *d, t_desc *list);
char			*ft_place_hash(char *data, t_desc *list);
char			*ft_strjoincl(char *s1, char *s2);
int				ft_last_flag(char *str);
char			*ft_takestring(const char *s, int p, t_desc *list);
char			*ft_parse_wchar(const uint32_t symbol, int p);
char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
int				ft_printf_atoi(char const *str, int pos);
t_desc			*ft_create_new_list(void);
char			*ft_make_prec(char *str, t_desc *list);
int				ft_check_how_many(char *s, int p, char c);
void			ft_check_flag(char *str, t_desc *list);
void			ft_check_width(char *s, t_desc *list);
void			ft_check_prec(char *s, t_desc *list);
void			ft_check_length(char *str, t_desc *list);
void			ft_putchar_check(t_desc *list, va_list arg);
void			ft_putstr_check(t_desc *list, va_list arg);
void			ft_base_16(t_desc *list, va_list arg);
void			ft_base_8(t_desc *list, va_list arg);
void			ft_unsigned_check(t_desc *list, va_list arg);
void			ft_regular_num(t_desc *list, va_list arg);
int				ft_printf(const char *format, ...);
void			ft_putstr(char *s);
char			*ft_itoa(int n);
char			*ft_itoa_base(uintmax_t value, t_desc *list, int b);
size_t			ft_strlen(char const *str);
size_t			ft_numlen(intmax_t nb);
void			ft_place_shift(int size);
char			*ft_place_zero(int size);
int				ft_output_len(const char *str, int pos);
void			ft_pointer_check(t_desc *list, va_list arg);

#endif
